ThunderBot Themes
A collection of themes for the ThunderBot by freshek and cryz35.

How to use?
1. Download the theme you'd like to use.
2. Locate your ThunderBot folder.
3. Add the theme to the "themes" folder.
4. Load it inside ThunderBot.
5. Enjoy your theme!